# Resources that I used

- https://www.telekom.de/hilfe/downloads/schnittstellenbeschreibung-1tr110-1
- https://www-user.tu-chemnitz.de/~heha/basteln/Haus/Telefon/Impulswahl%e2%86%92DTMF/
- https://www.kahlhans.de/telefonseite/galerie/fetap-611/
- https://elektronikbasteln.pl7.de/funktion-telefon

# Slides

## 1 The analog telephone circuit

![the analog telephone circuit](1-the-analog-telephone-circuit.png)

- subscriber line L_a, L_b
- AC alarm bell
- dial switch nsa, nsi
- voltage spike protection R
- cradle switch
- carbon microphone M
- speaker S
- clamping diode D
- transformer
- line impedance replication R_L + C_L

## 2 Speaker + microphone + cradle switch

![speaker + microphone + cradle switch](2-speaker+microphone+cradle-switch.png)

- on cradle -> subscriber line sends dc voltage of 30 - 60 V, historically 60 V because of long wires, my Vodafone cable modem has 48 V
- off cradle -> closes switch, current flows, voltage drops to 5 - 10 V -> signals connection, either to answer a call or to start dialing
- carbon microphone: speak -> resistance 1-10 kOhm -> 100 mV AC is imposed
- this is a working telephon! problem: you hear your own voice

## 3 Own voice suppression

![own voice suppression](3-own-voice-suppression.png)

- connect microphone to the middle of the primary winding of the transformer
- current I_M from microphone flows through both sides of the transformer and cancels itself out on the secondary winding (speaker side) -> suppresses your own voice
- line impedance replication: R2 = 1 kOhm, C2 = 100 nF
- primary winding: left 50 Ohm, right 200 Ohm, secondary: 60 Ohm
- clamping diodes, cut voltages above 400 mV -> ear protection!

## 4 Alarm

![alarm](4-alarm.png)

- on cradle -> switch open
- rings when line sends 60 V, 25 Hz AC, 20 mA
- C1 = 1 uF, R = 900 Ohm DC resistance, 3 kOhm @ 25 Hz, much higher at higher frequencies -> does not interfere with speech!

## 5 The Dial switch

![dial switch](5-dial-switch.png)

- take handle off cradle: switch closes, current flows, voltage drops
- not dialing: nsi closed and nsa open
- start dialing: nsa closes -> voltage drops to 0V, signals start dialing
- let go of dial: nsa stays closed, nsi repeatedly opens and closes for as many times as the dialed number, 62 ms open and 38 ms closed (+/- 5 ms), spring and centrifugal governor
- switched inductive load -> R1 = 100 Ohm for voltage spike protection of nsi switch
